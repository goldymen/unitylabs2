using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceVisual : MonoBehaviour
{
    public ResourceBank _resourcebank;
    public Text _resourceText;
    public GameResource _gameresource;

    void Update()
    {

        UpdateText(_resourceText, _resourcebank.GetResource(_gameresource));

    }

    public void UpdateText(Text text, int gameresource)
    {
       text.text = gameresource.ToString();
    }
}
