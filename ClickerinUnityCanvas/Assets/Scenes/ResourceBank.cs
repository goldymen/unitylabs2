using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ResourceBank : MonoBehaviour
{
    private Dictionary<GameResource, int> _gameresources;

    private void Awake()
    {
        _gameresources = new Dictionary<GameResource, int>//���������� ���
        {
            [GameResource.Food] = 0,
            [GameResource.Humans] = 0,
            [GameResource.Wood] = 0,
            [GameResource.Stone] = 0,
            [GameResource.Gold] = 0
        };
    }

    public void ChangeResource(GameResource r, int v)
    {
        if (!_gameresources.ContainsKey(r))//����� ����� ������{}
        {
            _gameresources.Add(r, v);
        }
        _gameresources[r] += v;
    }

    public int GetResource(GameResource r)
    {
        return _gameresources[r];
    }


}
