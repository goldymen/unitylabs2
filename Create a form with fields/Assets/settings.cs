using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class settings : MonoBehaviour
{

    [SerializeField] private TMP_InputField _nickField;
    [SerializeField] public FormView _FormView;
    [SerializeField] private GameObject _canvas;
    [SerializeField] public TextMeshProUGUI _Text;


    private bool active = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            active = !active;
            _canvas.gameObject.SetActive(active);
        }

        if (Input.GetKeyDown(KeyCode.Return) && active == true && _nickField.text == "")
        {
            _FormView.Name();
            _FormView.UpdateFields();
        }
        else
        {
            _Text.gameObject.SetActive(true);
        }



    }

}
