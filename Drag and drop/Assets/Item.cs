using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class Item : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{

    [SerializeField] private Sound _sound;
    [SerializeField] private RectTransform _increase;
    public void OnPointerDown(PointerEventData eventData)
    {
        _sound.PlayClickSound();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _increase.transform.localScale *= new Vector2(2f, 2f); ;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _increase.transform.localScale = new Vector2(1f, 1f);
    }
}
