using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perform : MonoBehaviour
{
    private IStrategy _strategy;
    [SerializeField] private Transform _transform;

    public void SetStratedy(IStrategy strategy){
        _strategy = strategy;
    }

    private void Update(){
        _strategy.Perform(_transform);
    }
}

