using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonComponent : MonoBehaviour
{
    [SerializeField] private Performer _performer;
    [SerializeField] private Button _button;
    public StrategyVariants StrategyVariant;
    public enum StrategyVariants
    {
        MoveForward,
        Rotate,
        Emmit
    }
    
    public void Awake()
    {
        _button.onClick.RemoveAllListeners();
        _button.onClick.AddListener(() =>
        {
            if(StrategyVariant == StrategyVariants.MoveForward)
            {
                _performer.SetStratedy(new MoveForward());
            }
            else if (StrategyVariant == StrategyVariants.Rotate)
            {
                _performer.SetStratedy(new Rotate());
            }
            else if(StrategyVariant == StrategyVariants.Emmit)
            {
                _performer.SetStratedy(new Emmit());
            }
        });
    } 
}
